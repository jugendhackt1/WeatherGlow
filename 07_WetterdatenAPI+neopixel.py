# Bibliotheken laden
import machine
import network
import time
import urequests
import neopixel

# Festlgung der Pin-Nummer, an dem der Neopixel-Streifen (Daten) angeschlossen ist
pin_nummer = 0

# Definition der Anzahl der Neopixel-LEDs in Ihrem Streifen
anzahl_leds = 10

# Definition der Status-LED, der internen LED
led_onboard = machine.Pin('LED', machine.Pin.OUT, value=0)

# Erstellen Sie eine Neopixel-Instanz mit der angegebenen Pin-Nummer und Anzahl der LEDs
np = neopixel.NeoPixel(machine.Pin(pin_nummer), anzahl_leds)

# Definieren Sie eine Liste mit 12 verschiedenen Farben (RGB Tertiärfarben, Farbkreis)
farben = [(255, 0, 0),  	# 0. Rot
          (255, 127, 0), 	# 1. Orange
          (255, 255, 0),  	# 2. Gelb
          (127, 255, 0),	# 3. Grün Gelb
          (0, 255, 0),		# 4. Grün
          (0, 255, 127), 	# 5. Mint Grün
          (0, 255, 255), 	# 6. Türkis
          (0, 127, 255),	# 7. Hellblau
          (0, 0, 255),  	# 8. Blau
          (127, 0, 255),	# 9. Violett
          (255, 0, 255), 	# 10. Pink
          (255, 0, 127),  	# 11. Magenta
          (255, 255, 255)]	# 12. Weiß

# WLAN-Konfiguration
wlanSSID = 'wlanSSID'
wlanPW = 'passwort'
network.country('DE')

# Funktion: WLAN-Verbindung
def wlanConnect():
    wlan = network.WLAN(network.STA_IF)
    if not wlan.isconnected():
        print('WLAN-Verbindung herstellen')
        wlan.active(True)
        wlan.connect(wlanSSID, wlanPW)
        for i in range(10):
            if wlan.status() == network.STAT_GOT_IP:  # Ändern Sie die Bedingung hier
                break
            led_onboard.toggle()
            print('.', wlan.status())
            time.sleep(1)
    if wlan.isconnected():
        print('WLAN-Verbindung hergestellt')
        led_onboard.on()
        print('WLAN-Status:', wlan.status())
        netConfig = wlan.ifconfig()
        print('IPv4-Adresse:', netConfig[0], '/', netConfig[1])
        print('Standard-Gateway:', netConfig[2])
        print('DNS-Server:', netConfig[3])
    else:
        print('Keine WLAN-Verbindung')
        led_onboard.off()
        print('WLAN-Status:', wlan.status())
    
    time.sleep(5)  # Wartezeit vor erneuter Überprüfung der Verbindung

# Set your latitude/longitude here (find yours by right clicking in Google Maps!)
LAT = 51.05089
LNG = 13.73832
TIMEZONE = "auto"  # determines time zone from lat/long

URL = "http://api.open-meteo.com/v1/forecast?latitude=" + str(LAT) + "&longitude=" + str(LNG) + "&current_weather=true&timezone=" + TIMEZONE
UPDATE_INTERVAL = 900  # refresh interval in secs. Be nice to free APIs!

# Weather codes from https://open-meteo.com/en/docs#:~:text=WMO%20Weather%20interpretation%20codes%20(WW)
WEATHERCODES = {
    0: 'clear sky',
    1: 'mostly clear',
    2: 'partly cloudy',
    3: 'cloudy',
    45: 'fog and depositing rime',
    48: 'fog',
    51: 'light drizzle',
    53: 'moderate drizzle',
    55: 'dense drizzle',
    56: 'light freezing drizzle',
    57: 'dense freezing drizzle',
    61: 'slight rain',
    63: 'moderate rain',
    65: 'heavy rain',
    66: 'light freezing rain',
    67: 'heavy freezing rain',
    71: 'slight snow',
    73: 'moderate snow',
    75: 'heavy snow',
    77: 'snow grains',
    80: 'slight rain showers',
    81: 'moderate rain showers',
    82: 'violent rain showers',
    85: 'slight snow showers',
    86: 'heavy snow showers',
    95: 'thunderstorm',
    96: 'thunderstorm with slight hail',
    99: 'thunderstorm with heavy hail'
}

def get_data():
    global weathercode
    print(f"Requesting URL: {URL}")
    r = urequests.get(URL)
    # open the json data
    j = r.json()
    print("Data obtained!")

    # parse relevant data from JSON
    current = j["current_weather"]
    temperature = current["temperature"]
    weathercode = current["weathercode"]
    #weathercode = 2 		#Test
    datetime_arr = current["time"].split("T")

    print(f"""Temperature = {temperature}°C
Conditions = {WEATHERCODES.get(weathercode, 'Unknown')}  # Verwenden Sie .get(), um unbekannte Wettercodes abzufangen
Last Open-Meteo update: {datetime_arr[0]}, {datetime_arr[1]}
    """)
    print(weathercode)
    r.close()

    # flash the onboard LED after getting data
    for i in range (5):
        led_onboard.value(True)  
        time.sleep(0.1)
        led_onboard.value(False)
        time.sleep(0.1)

#Hauptprogramm
while True:
    # WLAN-Verbindung herstellen
    wlanConnect()

    # get the first lot of data
    get_data()
    
    # Definition der Wetterdaten und Lichtfarbe
    if weathercode in (0, 1): 	# klar
        Farbe = 8 				# setze blau
    elif weathercode in (2, 3): # bewölkt
        Farbe = 12 				# setze weiß
    else:
        Farbe = 0 				# rot
    
    # Alle LEDs auf die gewünschte Farbe setzen
    for LED in range(anzahl_leds):
        np[LED] = farben[Farbe]
    np.write()  # Aktualisieren Sie die Neopixel-LEDs
    
    time.sleep(300)
