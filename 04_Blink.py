# Bibliotheken laden
from machine import Pin
from time import sleep

# Initialisierung der Onboard-LED
led_onboard = Pin("LED", Pin.OUT)

# Wiederholung (Endlos-Schleife)
while True:
    # LED einschalten
    led_onboard.on()
    # halbe Sekunde warten
    sleep(1)
    # LED ausschalten
    led_onboard.off()
    # 1 Sekunde warten
    sleep(1)