import machine
import neopixel
import time

# Pin-Nummer, an dem der Neopixel-Streifen angeschlossen ist
pin_nummer = 0

# Anzahl der Neopixel-LEDs in Ihrem Streifen
anzahl_leds = 10

# Erstellen Sie eine Neopixel-Instanz mit der angegebenen Pin-Nummer und Anzahl der LEDs
np = neopixel.NeoPixel(machine.Pin(pin_nummer), anzahl_leds)

# Definieren Sie eine Liste mit 12 verschiedenen Farben (RGB Tertiärfarben, Farbkreis)
farben = [(255, 0, 0),  	# 0. Rot
          (255, 127, 0), 	# 1. Orange
          (255, 255, 0),  	# 2. Gelb
          (127, 255, 0),	# 3. Grün Gelb
          (0, 255, 0),		# 4. Grün
          (0, 255, 127), 	# 5. Mint Grün
          (0, 255, 255), 	# 6. Türkis
          (0, 127, 255),	# 7. Hellblau
          (0, 0, 255),  	# 8. Blau
          (127, 0, 255),	# 9. Violett
          (255, 0, 255), 	# 10. Pink
          (255, 0, 127),  	# 11. Magenta
          (255, 255, 255)]	# 12. Weiß

#Farbe gem Liste (von 0 anfangen zu zählen)
Farbe = 8

# Alle LEDs auf die gewünschte Farbe setzen
for LED in range(anzahl_leds):
    np[LED] = farben[Farbe]

np.write()  # Aktualisieren Sie die Neopixel-LEDs
